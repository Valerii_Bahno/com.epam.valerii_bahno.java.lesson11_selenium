import com.epam.pages.FirstPage;
import com.epam.pages.SecondPage;
import com.epam.pages.ThirdPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;

public class TestSelenium {

    private WebDriver driver;
    private static final String googleUrl = "https://www.google.com.ua/";
    private static final String searchText = "Selenium";

    @Before
    public void start() {

        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(googleUrl);
    }

    @Test
    public void searchTitleSite() {

        FirstPage firstPage = new FirstPage(driver);
        SecondPage search = firstPage.search(searchText);
        ThirdPage result = search.goToSite();

        assertEquals("SeleniumHQ Browser Automation", result.getTitleSite());
    }

    @After
    public void close() {

        driver.quit();
    }
}


