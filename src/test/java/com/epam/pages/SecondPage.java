package com.epam.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SecondPage extends FirstPage {

    private static final String xpathSelenium = "//a[@href='https://www.selenium.dev/']/h3[@class='LC20lb DKV0Md']/span";

    private WebElement firstSite;

    public SecondPage(WebDriver driver) {
        super(driver);
    }

    public ThirdPage goToSite() {

        driver.findElement(By.xpath(xpathSelenium)).click();
        return new ThirdPage(driver);
    }
}
