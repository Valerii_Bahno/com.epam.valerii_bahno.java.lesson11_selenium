package com.epam.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FirstPage extends BasePage {

    @FindBy(xpath = "//input[@class='gLFyf gsfi']")
    private WebElement searchField;

    public FirstPage(WebDriver driver) {
        super(driver);
    }

    public SecondPage search(String searchText) {

        searchField.sendKeys(searchText, Keys.ENTER);
        return new SecondPage(driver);
    }
}
