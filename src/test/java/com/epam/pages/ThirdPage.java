package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ThirdPage extends SecondPage {

    private WebElement titleSite;

    public ThirdPage(WebDriver driver) {
        super(driver);
    }

    public String getTitleSite() {

        return driver.getTitle();
    }
}
